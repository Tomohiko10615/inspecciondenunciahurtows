//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.15 a las 12:02:28 PM COT 
//


package pe.com.enel.market.webServices.inspeccionDenunciaHurto.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DatosAutenticacionInspeccionType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosAutenticacionInspeccionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="llaveSecreta">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigoDistribuidora">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigoSistemaExterno">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigoTipodeTdC">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigoProceso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigoSubProceso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosAutenticacionInspeccionType", propOrder = {
    "llaveSecreta",
    "codigoDistribuidora",
    "codigoSistemaExterno",
    "codigoTipodeTdC",
    "codigoProceso",
    "codigoSubProceso"
})
public class DatosAutenticacionInspeccionType {

    @XmlElement(required = true)
    protected String llaveSecreta;
    @XmlElement(required = true)
    protected String codigoDistribuidora;
    @XmlElement(required = true)
    protected String codigoSistemaExterno;
    @XmlElement(required = true)
    protected String codigoTipodeTdC;
    @XmlElement(required = true)
    protected String codigoProceso;
    @XmlElement(required = true)
    protected String codigoSubProceso;

    /**
     * Obtiene el valor de la propiedad llaveSecreta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLlaveSecreta() {
        return llaveSecreta;
    }

    /**
     * Define el valor de la propiedad llaveSecreta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLlaveSecreta(String value) {
        this.llaveSecreta = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoDistribuidora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDistribuidora() {
        return codigoDistribuidora;
    }

    /**
     * Define el valor de la propiedad codigoDistribuidora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDistribuidora(String value) {
        this.codigoDistribuidora = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoSistemaExterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSistemaExterno() {
        return codigoSistemaExterno;
    }

    /**
     * Define el valor de la propiedad codigoSistemaExterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSistemaExterno(String value) {
        this.codigoSistemaExterno = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoTipodeTdC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTipodeTdC() {
        return codigoTipodeTdC;
    }

    /**
     * Define el valor de la propiedad codigoTipodeTdC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTipodeTdC(String value) {
        this.codigoTipodeTdC = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProceso() {
        return codigoProceso;
    }

    /**
     * Define el valor de la propiedad codigoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProceso(String value) {
        this.codigoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoSubProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSubProceso() {
        return codigoSubProceso;
    }

    /**
     * Define el valor de la propiedad codigoSubProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSubProceso(String value) {
        this.codigoSubProceso = value;
    }

}
