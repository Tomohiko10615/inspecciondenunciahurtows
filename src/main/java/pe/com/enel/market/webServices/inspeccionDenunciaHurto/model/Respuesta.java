package pe.com.enel.market.webServices.inspeccionDenunciaHurto.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Respuesta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nroOrden;
	private Long codInternoTdc;
	private String codResultado;
	private String descResultado;
}
