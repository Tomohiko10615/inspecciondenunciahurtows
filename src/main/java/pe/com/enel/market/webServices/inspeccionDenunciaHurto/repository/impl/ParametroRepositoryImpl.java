package pe.com.enel.market.webServices.inspeccionDenunciaHurto.repository.impl;

import java.sql.CallableStatement;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.internal.SessionImpl;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.beans.ParametroBean;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoRequest;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoResponse;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.model.DireccionInfo;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.model.OrdenInspeccion;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.model.Respuesta;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.repository.ParametroRepositoryCustom;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.util.ConstantesWS;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.util.Utilitario;

@Repository
public class ParametroRepositoryImpl implements ParametroRepositoryCustom {

	@PersistenceContext
	private EntityManager em;
	
	private static final String resourceUrl = "http://claapiapwj03:8080/pq-inspeccion-denuncia-hurto/InspeccionDenunciaHurto/ws/crear/orden/inspeccion";

	SessionImpl session;

	public InspeccionDenunciaHurtoResponse generarOrdenInspeccion(InspeccionDenunciaHurtoRequest request, InspeccionDenunciaHurtoResponse response, Map<String, Map<String, ParametroBean>> parametro) throws Exception {
		//session = em.unwrap(SessionImpl.class);
		RestTemplate restTemplate = new RestTemplate();
		
		DireccionInfo direccionInfo = DireccionInfo.builder()
				.nombreVia(StringUtils.trimToEmpty(request.getDatosDeDireccion().getNombreVia()))
				.tipoAgr(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoAgrupacion()))
				.preferencia(StringUtils.trimToEmpty(request.getDatosDeDireccion().getReferencia()))
				.peNumero(StringUtils.trimToEmpty(request.getDatosDeDireccion().getNroMunicipal()))
				.tipoSect(StringUtils.trimToEmpty(request.getDatosDeDireccion().getTipoSector()))
				.nombreAgr(StringUtils.trimToEmpty(request.getDatosDeDireccion().getNombreAgrupacion()))
				.textoInt(StringUtils.trimToEmpty(request.getDatosDeDireccion().getInterior()))
				.tipoInt(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoInterior()))
				.textoSect(StringUtils.trimToEmpty(request.getDatosDeDireccion().getTextoSector()))
				.provincia(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodProvincia()))
				.distrito(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodDistrito()))
				.tipoVia(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoVia()))
				.manzana(StringUtils.trimToEmpty(request.getDatosDeDireccion().getManzana()))
				.tipoNum(StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoNumero()))
				.lote(StringUtils.trimToEmpty(request.getDatosDeDireccion().getLote()))
				.direccion(StringUtils.trimToEmpty(request.getDatosDeDireccion().getDireccionTexto()))
				.build();

		HttpEntity<OrdenInspeccion> datosOrdenInspeccionrequest = new HttpEntity<>(OrdenInspeccion.builder()
				.codPartition(parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARTITION).getValorAlf())
				.username(parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_USUARIO).getValorAlf())
				.idState(parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_ESTADO).getValorAlf())
				.codSubtipoOrden(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodSubtipoOrden()))
				.autogenerada(parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_AUTOGENERADO).getValorAlf())
				.codMotivo(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodMotivo()))
				.codCuadrilla(StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodCuadrilla())) ? parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_CUADRILLA).getValorAlf() : StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodCuadrilla()))
				.nroCuenta(StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCuenta())) ? ConstantesWS.NUMBER_VACIO : Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCuenta())))
				.idTipoOrden(parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_TIPO_ORDEN).getValorNum())
				.direccionInfo(direccionInfo)
				.latitud(request.getDatosDeDireccion().getLatitud())
				.longitud(request.getDatosDeDireccion().getLongitud())
				.nroDenuncia(Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroDenuncia())))
				.nroCaso(Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCaso())))
				.nombre(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNombre()))
				.prioridad(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getPrioridad()))
				.fechaDenuncia(Utilitario.calendarToDate((request.getDatosDeInspeccion().getFechaDenuncia())))
				.observacion(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getObservacion()))
				.nroCuentaDenun(StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroCuentaDenunciante())) ? ConstantesWS.NUMBER_VACIO : Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroCuentaDenunciante())))
				.codTipoDoc(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getCodTipoDoc()))
				.nroDoc(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroDoc()))
				.build());
		Respuesta respuesta = restTemplate.postForObject(resourceUrl, datosOrdenInspeccionrequest, Respuesta.class);

		/*
		CallableStatement callableStatement;
		
		callableStatement = session.connection().prepareCall("CALL SYNERGIA.PQINSPECCION_DENUNCIA_HURTO.SP_CREAR_ORDER_INSPECCION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		callableStatement.setString(1, parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARTITION).getValorAlf());
		callableStatement.setString(2, parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_USUARIO).getValorAlf());
		callableStatement.setString(3, parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_ESTADO).getValorAlf());
		callableStatement.setString(4, StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodSubtipoOrden()));
		callableStatement.setString(5, parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_AUTOGENERADO).getValorAlf());
		callableStatement.setString(6, StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodMotivo()));
		callableStatement.setString(7, StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodCuadrilla())) ? parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_CUADRILLA).getValorAlf() : StringUtils.trimToEmpty(request.getDatosDeInspeccion().getCodCuadrilla()));
		callableStatement.setLong(8, StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCuenta())) ? ConstantesWS.NUMBER_VACIO : Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCuenta())));
		callableStatement.setLong(9, parametro.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_TIPO_ORDEN).getValorNum());
		callableStatement.setString(10, StringUtils.trimToEmpty(request.getDatosDeDireccion().getNombreVia()));
		callableStatement.setString(11, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoAgrupacion()));
		callableStatement.setString(12, StringUtils.trimToEmpty(request.getDatosDeDireccion().getReferencia()));
		callableStatement.setString(13, StringUtils.trimToEmpty(request.getDatosDeDireccion().getNroMunicipal()));
		callableStatement.setString(14, StringUtils.trimToEmpty(request.getDatosDeDireccion().getTipoSector()));
		callableStatement.setString(15, StringUtils.trimToEmpty(request.getDatosDeDireccion().getNombreAgrupacion()));
		callableStatement.setString(16, StringUtils.trimToEmpty(request.getDatosDeDireccion().getInterior()));
		callableStatement.setString(17, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoInterior()));
		callableStatement.setString(18, StringUtils.trimToEmpty(request.getDatosDeDireccion().getTextoSector()));
		callableStatement.setString(19, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodProvincia()));
		callableStatement.setString(20, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodDistrito()));
		callableStatement.setString(21, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoVia()));
		callableStatement.setString(22, StringUtils.trimToEmpty(request.getDatosDeDireccion().getManzana()));
		callableStatement.setString(23, StringUtils.trimToEmpty(request.getDatosDeDireccion().getCodTipoNumero()));
		callableStatement.setString(24, StringUtils.trimToEmpty(request.getDatosDeDireccion().getLote()));
		callableStatement.setString(25, StringUtils.trimToEmpty(request.getDatosDeDireccion().getDireccionTexto()));
		callableStatement.setBigDecimal(26, request.getDatosDeDireccion().getLatitud());
		callableStatement.setBigDecimal(27, request.getDatosDeDireccion().getLongitud());
		callableStatement.setLong(28, Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroDenuncia())));
		callableStatement.setLong(29, Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeInspeccion().getNroCaso())));
		callableStatement.setString(30, StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNombre()));
		callableStatement.setString(31, StringUtils.trimToEmpty(request.getDatosDeInspeccion().getPrioridad()));
		callableStatement.setDate(32, Utilitario.calendarToDate((request.getDatosDeInspeccion().getFechaDenuncia())));
		callableStatement.setString(33, StringUtils.trimToEmpty(request.getDatosDeInspeccion().getObservacion()));
		callableStatement.setLong(34, StringUtils.isEmpty(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroCuentaDenunciante())) ? ConstantesWS.NUMBER_VACIO : Long.parseLong(StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroCuentaDenunciante())));
		callableStatement.setString(35, StringUtils.trimToEmpty(request.getDatosDeDenunciante().getCodTipoDoc()));
		callableStatement.setString(36, StringUtils.trimToEmpty(request.getDatosDeDenunciante().getNroDoc()));
		callableStatement.executeUpdate();
		*/
		response.setNroOrdenInspeccion(respuesta.getNroOrden());
		response.setCodInternoTDC(respuesta.getCodInternoTdc() == null ? null : respuesta.getCodInternoTdc());
		response.setCodigoResultado(respuesta.getCodResultado());
		response.setDescripcionResultado(respuesta.getDescResultado());
		return response;
	}

}
