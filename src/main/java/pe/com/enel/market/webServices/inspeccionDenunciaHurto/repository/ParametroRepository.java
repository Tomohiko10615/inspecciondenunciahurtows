package pe.com.enel.market.webServices.inspeccionDenunciaHurto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.beans.ParametroBean;

public interface ParametroRepository extends JpaRepository<ParametroBean, Long>, ParametroRepositoryCustom {

	public List<ParametroBean> findAllBySistemaAndEntidadAndActivo(String sistema, String entidad, String activo);

	@Query(value = "Select 1 from dual", nativeQuery = true)
	public Long validarDB();

}
