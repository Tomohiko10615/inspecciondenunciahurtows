package pe.com.enel.market.webServices.inspeccionDenunciaHurto.service;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoRequest;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoResponse;

public interface InspeccionDenunciaHurtoService {

	InspeccionDenunciaHurtoResponse validarDB();

	InspeccionDenunciaHurtoResponse validarInput(InspeccionDenunciaHurtoRequest request);

	InspeccionDenunciaHurtoResponse getInspeccionDenunciaHurto(InspeccionDenunciaHurtoRequest request);

}
