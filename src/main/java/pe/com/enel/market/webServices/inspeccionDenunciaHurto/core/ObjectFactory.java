//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.15 a las 12:02:28 PM COT 
//


package pe.com.enel.market.webServices.inspeccionDenunciaHurto.core;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.com.enel.market.webServices.inspeccionDenunciaHurto.core package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.com.enel.market.webServices.inspeccionDenunciaHurto.core
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InspeccionDenunciaHurtoResponse }
     * 
     */
    public InspeccionDenunciaHurtoResponse createInspeccionDenunciaHurtoResponse() {
        return new InspeccionDenunciaHurtoResponse();
    }

    /**
     * Create an instance of {@link InspeccionDenunciaHurtoRequest }
     * 
     */
    public InspeccionDenunciaHurtoRequest createInspeccionDenunciaHurtoRequest() {
        return new InspeccionDenunciaHurtoRequest();
    }

    /**
     * Create an instance of {@link DatosAutenticacionInspeccionType }
     * 
     */
    public DatosAutenticacionInspeccionType createDatosAutenticacionInspeccionType() {
        return new DatosAutenticacionInspeccionType();
    }

    /**
     * Create an instance of {@link DatosInspeccionType }
     * 
     */
    public DatosInspeccionType createDatosInspeccionType() {
        return new DatosInspeccionType();
    }

    /**
     * Create an instance of {@link DatosDireccionType }
     * 
     */
    public DatosDireccionType createDatosDireccionType() {
        return new DatosDireccionType();
    }

    /**
     * Create an instance of {@link DatosDenuncianteType }
     * 
     */
    public DatosDenuncianteType createDatosDenuncianteType() {
        return new DatosDenuncianteType();
    }

}
