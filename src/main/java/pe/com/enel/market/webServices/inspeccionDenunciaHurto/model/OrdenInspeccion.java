package pe.com.enel.market.webServices.inspeccionDenunciaHurto.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrdenInspeccion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codPartition;
    private String username;
    private String idState;
    private String codSubtipoOrden;
    private String autogenerada;
    private String codMotivo;
    private String codCuadrilla;
    private Long nroCuenta;
    private Long idTipoOrden;
    private BigDecimal latitud;
    private BigDecimal longitud;
    private Long nroDenuncia;
    private Long nroCaso;
    private String nombre;
    private String prioridad;
    private Date fechaDenuncia;
    private String observacion;
    private Long nroCuentaDenun;
    private String codTipoDoc;
    private String nroDoc;
    private String nroOrden;
    private String codInternoTdc;
    private String codResultado;
    private String descResultado;
    private DireccionInfo direccionInfo;
}
