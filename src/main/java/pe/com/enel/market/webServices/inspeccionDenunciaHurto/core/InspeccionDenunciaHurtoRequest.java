//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.15 a las 12:02:28 PM COT 
//


package pe.com.enel.market.webServices.inspeccionDenunciaHurto.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatosAutenticacionInspeccion" type="{http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core}DatosAutenticacionInspeccionType"/>
 *         &lt;element name="datosDeInspeccion" type="{http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core}datosInspeccionType"/>
 *         &lt;element name="datosDeDireccion" type="{http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core}datosDireccionType" minOccurs="0"/>
 *         &lt;element name="datosDeDenunciante" type="{http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core}datosDenuncianteType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datosAutenticacionInspeccion",
    "datosDeInspeccion",
    "datosDeDireccion",
    "datosDeDenunciante"
})
@XmlRootElement(name = "inspeccionDenunciaHurtoRequest")
public class InspeccionDenunciaHurtoRequest {

    @XmlElement(name = "DatosAutenticacionInspeccion", required = true)
    protected DatosAutenticacionInspeccionType datosAutenticacionInspeccion;
    @XmlElement(required = true)
    protected DatosInspeccionType datosDeInspeccion;
    protected DatosDireccionType datosDeDireccion;
    protected DatosDenuncianteType datosDeDenunciante;

    /**
     * Obtiene el valor de la propiedad datosAutenticacionInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link DatosAutenticacionInspeccionType }
     *     
     */
    public DatosAutenticacionInspeccionType getDatosAutenticacionInspeccion() {
        return datosAutenticacionInspeccion;
    }

    /**
     * Define el valor de la propiedad datosAutenticacionInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosAutenticacionInspeccionType }
     *     
     */
    public void setDatosAutenticacionInspeccion(DatosAutenticacionInspeccionType value) {
        this.datosAutenticacionInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad datosDeInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link DatosInspeccionType }
     *     
     */
    public DatosInspeccionType getDatosDeInspeccion() {
        return datosDeInspeccion;
    }

    /**
     * Define el valor de la propiedad datosDeInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosInspeccionType }
     *     
     */
    public void setDatosDeInspeccion(DatosInspeccionType value) {
        this.datosDeInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad datosDeDireccion.
     * 
     * @return
     *     possible object is
     *     {@link DatosDireccionType }
     *     
     */
    public DatosDireccionType getDatosDeDireccion() {
        return datosDeDireccion;
    }

    /**
     * Define el valor de la propiedad datosDeDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosDireccionType }
     *     
     */
    public void setDatosDeDireccion(DatosDireccionType value) {
        this.datosDeDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad datosDeDenunciante.
     * 
     * @return
     *     possible object is
     *     {@link DatosDenuncianteType }
     *     
     */
    public DatosDenuncianteType getDatosDeDenunciante() {
        return datosDeDenunciante;
    }

    /**
     * Define el valor de la propiedad datosDeDenunciante.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosDenuncianteType }
     *     
     */
    public void setDatosDeDenunciante(DatosDenuncianteType value) {
        this.datosDeDenunciante = value;
    }

}
