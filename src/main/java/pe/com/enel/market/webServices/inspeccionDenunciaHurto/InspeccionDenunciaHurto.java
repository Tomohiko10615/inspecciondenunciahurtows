package pe.com.enel.market.webServices.inspeccionDenunciaHurto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
//@PropertySource(value = {"classpath:application.properties", "file:${spring.config.location}"})
public class InspeccionDenunciaHurto extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	private static Class<InspeccionDenunciaHurto> applicationClass = InspeccionDenunciaHurto.class;

}
