package pe.com.enel.market.webServices.inspeccionDenunciaHurto.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COM_PARAMETROS" , schema = "SYNERGIA")
public class ParametroBean {

	@Id
	@Column(name = "ID_PARAMETRO")
	private Long idParametro;

	@Column(name = "SISTEMA")
	private String sistema;

	@Column(name = "ENTIDAD")
	private String entidad;

	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "DESCRIPCION")
	private String descripcion;

	@Column(name = "VALOR_ALF")
	private String valorAlf;
	
	@Column(name = "VALOR_NUM")
	private Long valorNum;

	@Column(name = "ACTIVO")
	private String activo;

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getValorAlf() {
		return valorAlf;
	}

	public void setValorAlf(String valorAlf) {
		this.valorAlf = valorAlf;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getValorNum() {
		return valorNum;
	}

	public void setValorNum(Long valorNum) {
		this.valorNum = valorNum;
	}
}
