package pe.com.enel.market.webServices.inspeccionDenunciaHurto.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoRequest;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoResponse;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.service.InspeccionDenunciaHurtoService;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.util.ConstantesWS;

@Endpoint
public class InspeccionDenunciaHurtoEndpoint {

	private static final String NAMESPACE_URI = "http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core";

	@Autowired
	private InspeccionDenunciaHurtoService inspeccionDenunciaHurtoService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "inspeccionDenunciaHurtoRequest")
	@ResponsePayload
	public InspeccionDenunciaHurtoResponse consultarDeudaSedapal(@RequestPayload InspeccionDenunciaHurtoRequest request) {
		InspeccionDenunciaHurtoResponse response = inspeccionDenunciaHurtoService.validarDB();
		if (response.getCodigoResultado().equals(ConstantesWS.COD_SUCCESS)) {
			response = inspeccionDenunciaHurtoService.validarInput(request);
		}
		if (response.getCodigoResultado().equals(ConstantesWS.COD_SUCCESS)) {
			response = inspeccionDenunciaHurtoService.getInspeccionDenunciaHurto(request);
		}
		return response;
	}

}
