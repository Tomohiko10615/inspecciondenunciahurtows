package pe.com.enel.market.webServices.inspeccionDenunciaHurto.util;

import java.sql.Date;

import javax.xml.datatype.XMLGregorianCalendar;

public final class Utilitario {

	public static Date calendarToDate(XMLGregorianCalendar calendar) throws Exception {
		if (calendar == null) {
			return null;
		}
		return new Date((calendar.toGregorianCalendar().getTime()).getTime());
	}

}