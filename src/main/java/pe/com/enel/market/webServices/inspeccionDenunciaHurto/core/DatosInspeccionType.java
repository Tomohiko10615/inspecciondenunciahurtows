//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.15 a las 12:02:28 PM COT 
//


package pe.com.enel.market.webServices.inspeccionDenunciaHurto.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para datosInspeccionType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="datosInspeccionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroCuenta" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codSubtipoOrden">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codMotivo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codCuadrilla" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="fechaDenuncia">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}dateTime">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="prioridad">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nroCaso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="22"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nroDenuncia">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="22"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="observacion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="1200"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datosInspeccionType", propOrder = {
    "nroCuenta",
    "codSubtipoOrden",
    "codMotivo",
    "codCuadrilla",
    "fechaDenuncia",
    "prioridad",
    "nroCaso",
    "nroDenuncia",
    "observacion"
})
public class DatosInspeccionType {

    protected String nroCuenta;
    @XmlElement(required = true)
    protected String codSubtipoOrden;
    @XmlElement(required = true)
    protected String codMotivo;
    protected String codCuadrilla;
    @XmlElement(required = true)
    protected XMLGregorianCalendar fechaDenuncia;
    @XmlElement(required = true)
    protected String prioridad;
    @XmlElement(required = true)
    protected String nroCaso;
    @XmlElement(required = true)
    protected String nroDenuncia;
    protected String observacion;

    /**
     * Obtiene el valor de la propiedad nroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCuenta() {
        return nroCuenta;
    }

    /**
     * Define el valor de la propiedad nroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCuenta(String value) {
        this.nroCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad codSubtipoOrden.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSubtipoOrden() {
        return codSubtipoOrden;
    }

    /**
     * Define el valor de la propiedad codSubtipoOrden.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSubtipoOrden(String value) {
        this.codSubtipoOrden = value;
    }

    /**
     * Obtiene el valor de la propiedad codMotivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMotivo() {
        return codMotivo;
    }

    /**
     * Define el valor de la propiedad codMotivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMotivo(String value) {
        this.codMotivo = value;
    }

    /**
     * Obtiene el valor de la propiedad codCuadrilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCuadrilla() {
        return codCuadrilla;
    }

    /**
     * Define el valor de la propiedad codCuadrilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCuadrilla(String value) {
        this.codCuadrilla = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDenuncia() {
        return fechaDenuncia;
    }

    /**
     * Define el valor de la propiedad fechaDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDenuncia(XMLGregorianCalendar value) {
        this.fechaDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad prioridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrioridad() {
        return prioridad;
    }

    /**
     * Define el valor de la propiedad prioridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrioridad(String value) {
        this.prioridad = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCaso() {
        return nroCaso;
    }

    /**
     * Define el valor de la propiedad nroCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCaso(String value) {
        this.nroCaso = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDenuncia() {
        return nroDenuncia;
    }

    /**
     * Define el valor de la propiedad nroDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDenuncia(String value) {
        this.nroDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad observacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * Define el valor de la propiedad observacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacion(String value) {
        this.observacion = value;
    }

}
