package pe.com.enel.market.webServices.inspeccionDenunciaHurto.util;

public class ConstantesWS {
	// CODIGOS DE MENSAJE 
	public static final String COD_SUCCESS                       = "00";
	public static final String COD_ERROR_XSD_ESTRUCTURA          = "1001";
	public static final String COD_ERROR_BD                      = "1002";
	public static final String COD_ERROR_BD_INTERNO              = "1003";
	public static final String COD_BD_ORDEN_EXISTENTE            = "1004";
	public static final String COD_AUTENTICACION                 = "1005";
	public static final String COD_TIEMPO_LIMITE_INSPECCION      = "1006";
	public static final String COD_UBICACION_MINIMO_FICTICIO     = "1007";
	public static final String COD_ERROR_BD_DATOS                = "1008";
	public static final String COD_ERROR_GENERICO                = "2000";

	// DESCRIPCION DE MENSAJE
	public static final String COD_SUCCESS_DESCRIPTION                   = "Operación ejecutada con éxito";
	public static final String COD_ERROR_XSD_ESTRUCTURA_DESCRIPTION      = "Error en los campos de entrada del WS no respetan la estructura definida";
	public static final String COD_ERROR_BD_DESCRIPTION                  = "Error en la conexión a la base de datos";
	public static final String COD_ERROR_BD_INTERNO_DESCRIPTION          = "Error interno en la base de datos";
	public static final String COD_BD_ORDEN_EXISTENTE_DESCRIPTION        = "Ya existe una orden de inspección para la cuenta";
	public static final String COD_AUTENTICACION_DESCRIPTION             = "Los datos de autenticación no son válidos";
	public static final String COD_TIEMPO_LIMITE_INSPECCION_DESCRIPTION  = "Existe una orden de inspección para la cuenta, recientemente finalizada";
	public static final String COD_UBICACION_MINIMO_FICTICIO_DESCRIPTION = "Se requiere datos de dirección y/o datos de geolocalización";
	public static final String COD_ERROR_BD_DATOS_DESCRIPTION            = "Error de ingreso de datos";
	public static final String COD_ERROR_GENERICO_DESCRIPTION            = "Error inesperado";

	// Generacion de Orden de Denuncia
	public static final String COD_PARTITION          = "CODPARTI";
	public static final String COD_USUARIO            = "USERNAME";
	public static final String COD_ESTADO             = "STATEWKF";
	public static final String COD_TIPO_ORDEN         = "CODTIPOORD";
	public static final String COD_AUTOGENERADO       = "STATEAUTO";
	public static final String COD_CUADRILLA          = "CODCUADRI";
	public static final String PARAMETROS_SISTEMA     = "EORDER";
	public static final String PARAMETROS_ENTIDAD     = "WS_CREA_INSP";
	public static final String PARAMETROS_ACTIVO      = "S";
	public static final String COD_PARAMETROS_LLAVE   = "KEY";
	public static final String COD_PARAMETROS_DIST    = "CODDIST";
	public static final String COD_PARAMETROS_EXT     = "CODSISTEXT";
	public static final String COD_PARAMETROS_TDC     = "CODTIPOTDC";
	public static final String COD_PARAMETROS_PROC    = "CODPROCESO";
	public static final String COD_PARAMETROS_SUBPROC = "CODSUBPROC";
	public static final Long   NUMBER_VACIO           = -1L;
	public static final Long   NUMBER_ZERO            =  0L;

}
