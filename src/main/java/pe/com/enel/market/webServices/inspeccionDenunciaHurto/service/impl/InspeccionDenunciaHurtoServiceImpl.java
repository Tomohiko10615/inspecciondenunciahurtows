package pe.com.enel.market.webServices.inspeccionDenunciaHurto.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.beans.ParametroBean;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoRequest;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoResponse;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.repository.ParametroRepository;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.service.InspeccionDenunciaHurtoService;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.util.ConstantesWS;

@Service
public class InspeccionDenunciaHurtoServiceImpl implements InspeccionDenunciaHurtoService {

	@Autowired
	private ParametroRepository paramRepo;

	private final static Logger LOGGER = Logger.getLogger(InspeccionDenunciaHurtoServiceImpl.class);

	public Map<String, Map<String, ParametroBean>> params = new HashMap<String, Map<String, ParametroBean>>();

	@Transactional
	public InspeccionDenunciaHurtoResponse getInspeccionDenunciaHurto(InspeccionDenunciaHurtoRequest request) {
		InspeccionDenunciaHurtoResponse response = new InspeccionDenunciaHurtoResponse();
		setearResponse(response, ConstantesWS.COD_SUCCESS, ConstantesWS.COD_SUCCESS_DESCRIPTION);

		try {

			if (!validarAutenticacion(request, response)) {
				return response;
			}

			response = paramRepo.generarOrdenInspeccion(request, response, params);
			System.out.println("CodigoResultado: " + response.getCodigoResultado());
			System.out.println("DescripcionResultado: " + response.getDescripcionResultado());
			System.out.println("NroOrdenInspeccion: " + response.getNroOrdenInspeccion());

			switch (response.getCodigoResultado()) {
			case ConstantesWS.COD_BD_ORDEN_EXISTENTE:
				setearResponse(response, ConstantesWS.COD_BD_ORDEN_EXISTENTE, ConstantesWS.COD_BD_ORDEN_EXISTENTE_DESCRIPTION);
				return response;
			case ConstantesWS.COD_ERROR_BD_DATOS:
				setearResponse(response, ConstantesWS.COD_ERROR_BD_DATOS, ConstantesWS.COD_ERROR_BD_DATOS_DESCRIPTION + " - " + response.getDescripcionResultado());
				return response;
			case ConstantesWS.COD_ERROR_BD_INTERNO:
				setearResponse(response, ConstantesWS.COD_ERROR_BD_INTERNO, ConstantesWS.COD_ERROR_BD_INTERNO_DESCRIPTION + " - " + response.getDescripcionResultado());
				return response;
			case ConstantesWS.COD_TIEMPO_LIMITE_INSPECCION:
				setearResponse(response, ConstantesWS.COD_TIEMPO_LIMITE_INSPECCION, ConstantesWS.COD_TIEMPO_LIMITE_INSPECCION_DESCRIPTION);
				return response;
			case ConstantesWS.COD_UBICACION_MINIMO_FICTICIO:
				setearResponse(response, ConstantesWS.COD_UBICACION_MINIMO_FICTICIO, ConstantesWS.COD_UBICACION_MINIMO_FICTICIO_DESCRIPTION);
				return response;

			}

		} catch (Exception e) {
			String msg = e.toString();
			setearResponse(response, ConstantesWS.COD_ERROR_GENERICO, ConstantesWS.COD_ERROR_GENERICO_DESCRIPTION + " - " + msg.substring(0, msg.length()));
			LOGGER.error(e.getMessage(), e);
		}
		return response;
	}

	public InspeccionDenunciaHurtoResponse validarInput(InspeccionDenunciaHurtoRequest request) {
		InspeccionDenunciaHurtoResponse response = new InspeccionDenunciaHurtoResponse();
		response.setCodigoResultado(ConstantesWS.COD_SUCCESS);
		try {
			JAXBContext jc = JAXBContext.newInstance(InspeccionDenunciaHurtoRequest.class);
			JAXBSource source = new JAXBSource(jc, request);
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			Schema schema = sf.newSchema(classloader.getResource("inspeccionDenunciaHurto.xsd"));
			Validator validator = schema.newValidator();
			validator.validate(source);
		} catch (Exception e) {
			setearResponse(response, ConstantesWS.COD_ERROR_XSD_ESTRUCTURA, ConstantesWS.COD_ERROR_XSD_ESTRUCTURA_DESCRIPTION);
			LOGGER.error(e.getMessage(), e);
		}
		return response;
	}

	private boolean validarAutenticacion(InspeccionDenunciaHurtoRequest request, InspeccionDenunciaHurtoResponse response) {
		boolean isAutenticado = true;
		try {
			params = llenarParametros();
			if (!(params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_LLAVE).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getLlaveSecreta()))
					&& params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_DIST).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getCodigoDistribuidora()))
					&& params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_SUBPROC).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getCodigoSubProceso()))
					&& params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_TDC).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getCodigoTipodeTdC()))
					&& params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_PROC).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getCodigoProceso()))
					&& params.get(ConstantesWS.PARAMETROS_ENTIDAD).get(ConstantesWS.COD_PARAMETROS_EXT).getValorAlf().equalsIgnoreCase(StringUtils.trimToEmpty(request.getDatosAutenticacionInspeccion().getCodigoSistemaExterno())))) {
				setearResponse(response, ConstantesWS.COD_AUTENTICACION, ConstantesWS.COD_AUTENTICACION_DESCRIPTION);
				return false;
			}
		} catch (Exception e) {
			setearResponse(response, ConstantesWS.COD_ERROR_BD_INTERNO, ConstantesWS.COD_ERROR_BD_INTERNO);
			LOGGER.error(e.getMessage(), e);
		}
		return isAutenticado;
	}

	public InspeccionDenunciaHurtoResponse validarDB() {
		InspeccionDenunciaHurtoResponse response = new InspeccionDenunciaHurtoResponse();
		setearResponse(response, ConstantesWS.COD_SUCCESS, ConstantesWS.COD_SUCCESS_DESCRIPTION);
		try {
			Long db = (Long) paramRepo.validarDB();
			if (db == null || db.equals(new Long(0))) {
				setearResponse(response, ConstantesWS.COD_ERROR_BD, ConstantesWS.COD_ERROR_BD_DESCRIPTION);
			}
		} catch (Exception e) {
			setearResponse(response, ConstantesWS.COD_ERROR_BD, ConstantesWS.COD_ERROR_BD_DESCRIPTION);
			LOGGER.error(e.getMessage(), e);
		}
		return response;
	}

	private Map<String, Map<String, ParametroBean>> llenarParametros() throws Exception {
		Map<String, Map<String, ParametroBean>> map = new HashMap<String, Map<String, ParametroBean>>();

		List<ParametroBean> list = paramRepo.findAllBySistemaAndEntidadAndActivo(ConstantesWS.PARAMETROS_SISTEMA, ConstantesWS.PARAMETROS_ENTIDAD, ConstantesWS.PARAMETROS_ACTIVO);
		for (ParametroBean param : list) {
			if (map.get(param.getEntidad()) == null || map.get(param.getEntidad()).isEmpty()) {
				map.put(param.getEntidad(), new HashMap<String, ParametroBean>());
			}
			map.get(param.getEntidad()).put(param.getCodigo(), param);
		}

		return map;
	}

	private void setearResponse(InspeccionDenunciaHurtoResponse response, String codErrorMapeo, String message) {
		response.setCodigoResultado(codErrorMapeo);
		response.setDescripcionResultado(message + "");
		if (message != null && !message.isEmpty() && message.length() > 200) {
			response.setDescripcionResultado(message.substring(0, 200));
		}
	}

}
