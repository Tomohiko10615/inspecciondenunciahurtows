package pe.com.enel.market.webServices.inspeccionDenunciaHurto.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DireccionInfo {

	private String nombreVia;
	private String peNumero;
    private String tipoAgr;
    private String provincia;
    private String distrito;
    private String manzana;
    private String preferencia;
    private String tipoSect;
    private String nombreAgr;
    private String textoInt;
    private String tipoInt;
    private String textoSect;
    private String tipoVia;
    private String tipoNum;
    private String lote;
    private String direccion;
}
