//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.15 a las 12:02:28 PM COT 
//


package pe.com.enel.market.webServices.inspeccionDenunciaHurto.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoResultado">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="descripcionResultado">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nroOrdenInspeccion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codInternoTDC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}long">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codigoResultado",
    "descripcionResultado",
    "nroOrdenInspeccion",
    "codInternoTDC"
})
@XmlRootElement(name = "inspeccionDenunciaHurtoResponse")
public class InspeccionDenunciaHurtoResponse {

    @XmlElement(required = true)
    protected String codigoResultado;
    @XmlElement(required = true)
    protected String descripcionResultado;
    protected String nroOrdenInspeccion;
    protected Long codInternoTDC;

    /**
     * Obtiene el valor de la propiedad codigoResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResultado() {
        return codigoResultado;
    }

    /**
     * Define el valor de la propiedad codigoResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResultado(String value) {
        this.codigoResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionResultado() {
        return descripcionResultado;
    }

    /**
     * Define el valor de la propiedad descripcionResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionResultado(String value) {
        this.descripcionResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad nroOrdenInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroOrdenInspeccion() {
        return nroOrdenInspeccion;
    }

    /**
     * Define el valor de la propiedad nroOrdenInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroOrdenInspeccion(String value) {
        this.nroOrdenInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codInternoTDC.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodInternoTDC() {
        return codInternoTDC;
    }

    /**
     * Define el valor de la propiedad codInternoTDC.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodInternoTDC(Long value) {
        this.codInternoTDC = value;
    }

}
