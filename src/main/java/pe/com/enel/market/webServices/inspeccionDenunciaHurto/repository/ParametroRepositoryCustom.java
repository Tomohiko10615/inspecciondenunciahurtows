package pe.com.enel.market.webServices.inspeccionDenunciaHurto.repository;

import java.util.Map;

import pe.com.enel.market.webServices.inspeccionDenunciaHurto.beans.ParametroBean;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoRequest;
import pe.com.enel.market.webServices.inspeccionDenunciaHurto.core.InspeccionDenunciaHurtoResponse;

public interface ParametroRepositoryCustom {

	public InspeccionDenunciaHurtoResponse generarOrdenInspeccion(InspeccionDenunciaHurtoRequest request, InspeccionDenunciaHurtoResponse response, Map<String, Map<String, ParametroBean>> parametro) throws Exception;
}
