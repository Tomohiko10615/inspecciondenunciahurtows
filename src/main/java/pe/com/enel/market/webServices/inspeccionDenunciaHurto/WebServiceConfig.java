package pe.com.enel.market.webServices.inspeccionDenunciaHurto;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig {

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/ws/*");
	}

	@Bean(name = "inspecciondenunciahurto")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema resumenPagoSynergiaSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("InspeccionDenunciaHurtoPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setTargetNamespace("http://enel.com.pe/market/webServices/inspeccionDenunciaHurto/core");
		wsdl11Definition.setSchema(resumenPagoSynergiaSchema);
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema ConsultarDeudaSedapalSchema() {
		return new SimpleXsdSchema(new ClassPathResource("inspeccionDenunciaHurto.xsd"));
	}

}
